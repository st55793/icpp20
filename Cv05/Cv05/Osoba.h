#ifndef __OSOBA_H
#define __OSOBA_H
#include<string>

namespace Entity {
	struct Osoba {
	private:
		int id;
		std::string jmeno;
		std::string telefon;
	public:
		Osoba();
		Osoba(int id, std::string jmeno, std::string telefon);
		~Osoba();
		int getId() const;
		std::string getJmeno() const;
		std::string getTelefon() const;
	};
}

#endif
