#include "Monstrum.h"

int Monstrum::getHp() const
{
	return this->hp;
}

void Monstrum::setHp(int hp)
{
	this->hp = hp;
}

int Monstrum::getMaxhp() const
{
	return this->maxhp;
}

void Monstrum::setMaxhp(int maxhp)
{
	this->maxhp = maxhp;
}

