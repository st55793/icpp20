#ifndef __MATICE_H
#define __MATICE_H

template<typename T>
struct Matice {
private:
	int pocetRadku;
	int pocetSloupcu;
	T** data;
public:
	Matice(int radky, int sloupce) {
		this->pocetRadku = radky;
		this->pocetSloupcu = sloupce;
		this->data = new T * [this->pocetRadku];
		for (int i = 0; i < this->pocetRadku; i++)
		{
			this->data[i] = new T[this->pocetSloupcu];
		}
	}
	Matice(const Matice<T>& m) {
		this->pocetRadku = m.pocetRadku;
		this->pocetSloupcu = m.pocetSloupcu;
		this->data = new T * [this->pocetRadku];
		for (int i = 0; i < this->pocetRadku; i++)
		{
			this->data[i] = new T[this->pocetSloupcu];
			for (int j = 0; j < this->pocetSloupcu; j++)
			{
				this->data[i][j] = m.data[i][j];
			}
		}
	}
	~Matice() {
		for (int i = 0; i < this->pocetRadku; i++)
		{
			delete[] this->data[i];
		}
		delete[] this->data;
	}
	void Nastav(int radek, int sloupec, T hodnota);
	void NastavZ(T* pole);
	T& Dej(int radek, int sloupec);
	const T& Dej(int radek, int sloupec) const;
	template<typename R>
	Matice<R> Pretypuj() const;
	Matice Transpozice() const;
	Matice Soucin(const Matice& m) const;
	Matice Soucin(T skalar) const;
	Matice Soucet(const Matice& m) const;
	Matice Soucet(T skalar) const;
	bool JeShodna(const Matice& m) const;
	void Vypis() const;
};

template<typename T>
void Matice<T>::Nastav(int radek, int sloupec, T hodnota) {
	this->data[radek][sloupec] = hodnota;
}

template<typename T>
void Matice<T>::NastavZ(T* pole) {
	int count = 0;
	for (int i = 0; i < this->pocetRadku; i++)
	{
		for (int j = 0; j < this->pocetSloupcu; j++)
		{
			this->data[i][j] = pole[count];
			count++;
		}
	}
}

template<typename T>
T& Matice<T>::Dej(int radek, int sloupec) {
	if (radek > this->pocetRadku || sloupec > this->pocetSloupcu)
	{
		throw std::invalid_argument;
	}
	return this->data[radek][sloupec];
}

template<typename T>
const T& Matice<T>::Dej(int radek, int sloupec) const {
	if (radek > this->pocetRadku || sloupec > this->pocetSloupcu)
	{
		throw std::invalid_argument;
	}
	return this->data[radek][sloupec];
}

template<typename T>
template<typename R>
Matice<R> Matice<T>::Pretypuj() const
{
	Matice<R> matice = *this;
	for (int i = 0; i < matice.pocetRadku; i++)
	{
		for (int j = 0; j < matice.pocetSloupcu; j++)
		{
			matice->data[i][j] = (R)this->data[i][j];
		}
	}
	return matice;
}

template<typename T>
Matice<T> Matice<T>::Transpozice() const
{
	return nullptr;
}

template<typename T>
Matice<T> Matice<T>::Soucin(const Matice& m) const
{
	return nullptr;
}

template<typename T>
Matice<T> Matice<T>::Soucin(T skalar) const
{
	Matice<T> matice = *this;
	for (int i = 0; i < matice.pocetRadku; i++)
	{
		for (int j = 0; j < matice.pocetSloupcu; j++)
		{
			matice.data[i][j] *= skalar;
		}
	}
	return matice;
}
//
template<typename T>
Matice<T> Matice<T>::Soucet(const Matice& m) const
{
	Matice<T> matice = *this;
	for (int i = 0; i < matice.pocetRadku; i++)
	{
		for (int j = 0; j < matice.pocetSloupcu; j++)
		{
			matice.data[i][j] += m.data[i][j];
		}
	}
	return matice;
}

template<typename T>
Matice<T> Matice<T>::Soucet(T skalar) const
{
	Matice<T> matice = *this;
	for (int i = 0; i < matice.pocetRadku; i++)
	{
		for (int j = 0; j < matice.pocetSloupcu; j++)
		{
			matice.data[i][j] += skalar;
		}
	}
	return matice;
}

template<typename T>
bool Matice<T>::JeShodna(const Matice& m) const
{
	for (int i = 0; i < this->pocetRadku; i++)
	{
		for (int j = 0; j < this->pocetSloupcu; j++)
		{
			if (this->data[i][j] != m.data[i][j])
			{
				return false;
			}
		}
	}
	return true;
}

template<typename T>
void Matice<T>::Vypis() const
{
	std::cout << "Vypis matice:" << std::endl;
	for (int i = 0; i < this->pocetRadku; i++)
	{
		for (int j = 0; j < this->pocetSloupcu; j++)
		{
			std::cout << this->data[i][j] << " ";
		}
		std::cout << std::endl;
	}
}

#endif
