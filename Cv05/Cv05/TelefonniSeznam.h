#ifndef __TELEFONNISEZNAM_H
#define __TELEFONNISEZNAM_H
#include"Osoba.h"

namespace Model {
	struct TelefonniSeznam {
	public:
		TelefonniSeznam();
		~TelefonniSeznam();
		void pridejOsobu(Entity::Osoba o);
		std::string najdiTelefon(std::string jmeno) const;
		std::string najdiTelefon(int id) const;
	private:
		struct PrvekSeznamu {
			PrvekSeznamu();
			~PrvekSeznamu();
			PrvekSeznamu* dalsi;
			Entity::Osoba data;
		};
		PrvekSeznamu* _zacatek;
	};
}

#endif
