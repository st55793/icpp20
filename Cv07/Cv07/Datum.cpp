#include "Datum.h"

Datum::Datum()
{
	this->_den = 0;
	this->_mesic = 0;
	this->_rok = 0;
}

Datum::Datum(int den, int mesic, int rok)
{
	this->_den = den;
	this->_mesic = mesic;
	this->_rok = rok;
}

Datum::~Datum()
{
}

void Datum::setDen(int den)
{
	this->_den = den;
}

void Datum::setMesic(int mesic)
{
	this->_mesic = mesic;
}

void Datum::setRok(int rok)
{
	this->_rok = rok;
}

int Datum::getDen() const
{
	return this->_den;
}

int Datum::getMesic() const
{
	return this->_mesic;
}

int Datum::getRok() const
{
	return this->_rok;
}

std::ostream& operator<<(std::ostream& os, const Datum& datum)
{
	os << datum.getDen() << " " << datum.getMesic() << " " << datum.getRok();
	return os;
}

std::istream& operator>>(std::istream& is, Datum& datum)
{
	int den, mesic, rok;
	is >> den;
	datum.setDen(den);
	is >> mesic;
	datum.setMesic(mesic);
	is >> rok;
	datum.setRok(rok);
	return is;
}
