#include <iostream>

//Vytvo�te program, kter� z kl�vesnice na�te strany troj�heln�ka a, b, c.
//Provede test, zda jde sestrojit (a+b > c,  a+c > b,  b+c > a).
//Pokud jde sestrojit, pak vypi�te jeho obvod. V opa�n�m p��pad� oznamte
//u�ivateli, �e takov� troj�heln�k sestrojit nelze.

using namespace std;

int main()
{
	std::cout << "Hello World!\n";
	unsigned int a, b, c;
	a = b = c = 0;

	std::cout << "Zadejte stranu a: " << std::endl;
	std::cin >> a;
	std::cout << "Zadejte stranu b: " << std::endl;
	std::cin >> b;
	std::cout << "Zadejte stranu c: " << std::endl;
	std::cin >> c;

	if (!(a + b > c && a + c > b && b + c > a)) {
		std::cout << "Trojuhelnik nelze sestrojit (error exit code 1)" << std::endl;
		return 1;
	}

	unsigned int o = a + b + c;
	std::cout << "Obvod trojuhelniku je:" << o << std::endl;

	return 0;
}