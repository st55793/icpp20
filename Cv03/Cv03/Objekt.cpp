#include "Objekt.h"

Objekt::Objekt(int id)
{
	this->id = id;
}

Objekt::~Objekt()
{
}

int Objekt::getId() const
{
	return this->id;
}

double Objekt::getX() const
{
	return this->x;
}

void Objekt::setX(double x)
{
	this->x = x;
}

double Objekt::getY() const
{
	return this->y;
}

void Objekt::setY(double y)
{
	this->y = y;
}
