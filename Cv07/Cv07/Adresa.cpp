#include "Adresa.h"

Adresa::Adresa()
{
}

Adresa::Adresa(std::string ulice, std::string mesto, int psc)
{
	this->_ulice = ulice;
	this->_mesto = mesto;
	this->_psc = psc;
}

Adresa::~Adresa()
{
}

void Adresa::setUlice(std::string ulice)
{
	this->_ulice = ulice;
}

void Adresa::setMesto(std::string mesto)
{
	this->_mesto = mesto;
}

void Adresa::setPsc(int psc)
{
	this->_psc = psc;
}

std::string Adresa::getUlice() const
{
	return this->_ulice;
}

std::string Adresa::getMesto() const
{
	return this->_mesto;
}

int Adresa::getPsc() const
{
	return this->_psc;
}

std::ostream& operator<<(std::ostream& os, const Adresa& adresa)
{
	os << adresa.getUlice() << " " << adresa.getMesto() << " " << adresa.getPsc();
	return os;
}

std::istream& operator>>(std::istream& is, Adresa& adresa)
{
	std::string ulice, mesto;
	int psc;
	is >> ulice;
	adresa.setUlice(ulice);
	is >> mesto;
	adresa.setMesto(mesto);
	is >> psc;
	adresa.setPsc(psc);
	return is;
}
