#include <iostream>

using namespace std;

typedef struct Trojuhelnik {
	unsigned int a, b, c;
} Trojuhelnik;


bool lzeSestrojit(Trojuhelnik* t) {
	if ((t->a + t->b > t->c && t->a + t->c > t->b && t->b + t->c > t->a)) {
		return true;
	}
	else {
		return false;
	}
}

int main()
{
	Trojuhelnik* t = new Trojuhelnik;

	std::cout << "Zadejte stranu a: " << std::endl;
	std::cin >> t->a;
	std::cout << "Zadejte stranu b: " << std::endl;
	std::cin >> t->b;
	std::cout << "Zadejte stranu c: " << std::endl;
	std::cin >> t->c;

	if (!lzeSestrojit) {
		std::cout << "Trojuhelnik nelze sestrojit (error exit code 1)" << std::endl;
		return 1;
	}

	unsigned int o = t->a + t->b + t->c;
	std::cout << "Obvod trojuhelniku je:" << o << std::endl;

	return 0;
}