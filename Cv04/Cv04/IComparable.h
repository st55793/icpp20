#ifndef _COMP_H_
#define _COMP_H_
#include "IObject.h"

struct IComparable : IObject {
	virtual int compareTo(IComparable* obj) const = 0;
};

#endif
