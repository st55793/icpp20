#include<iostream>
#include"TelefonniSeznam.h"
#include"Osoba.h"
#include<string>

using namespace std;

int main() {
	Model::TelefonniSeznam* telefonniSeznam = new Model::TelefonniSeznam{};
	telefonniSeznam->pridejOsobu(*(new Entity::Osoba{ 0, "Tomas", "100" }));
	telefonniSeznam->pridejOsobu(*(new Entity::Osoba{ 1, "Filip", "200" }));
	telefonniSeznam->pridejOsobu(*(new Entity::Osoba{ 2, "Matej", "300" }));

	try
	{
		cout << telefonniSeznam->najdiTelefon("200") << endl;
	}
	catch (const std::invalid_argument&)
	{
		cout << "Neplatny vstupni parametr" << endl;
	}
	catch (const std::exception&)
	{
		cout << "Objekt nebyl nalezen" << endl;
	}

	cin.get();
	return 0;
}
