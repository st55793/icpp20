#ifndef __ADRESA_H
#define __ADRESA_H

#include<string>
#include<iostream>

struct Adresa {
private:
	std::string _ulice;
	std::string _mesto;
	int _psc;
public:
	Adresa();
	Adresa(std::string ulice, std::string mesto, int psc);
	~Adresa();
	void setUlice(std::string ulice);
	void setMesto(std::string mesto);
	void setPsc(int psc);
	std::string getUlice() const;
	std::string getMesto() const;
	int getPsc() const;
};

std::ostream& operator<<(std::ostream& os, const Adresa& adresa);
std::istream& operator>>(std::istream& is, Adresa& adresa);

#endif
