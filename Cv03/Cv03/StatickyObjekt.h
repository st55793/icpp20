#ifndef __STATICKYOBJEKT_H
#define __STATICKYOBJEKT_H
#include "Objekt.h"
#include "TypPrekazky.h"

class StatickyObjekt : virtual public Objekt
{
private:
	TypPrekazky typPrekazky;
public:
	StatickyObjekt(int, TypPrekazky);
	TypPrekazky getTypPrekazky() const;
};
#endif
