#ifndef __MONSTRUM_H
#define __MONSTRUM_H
#include "PohyblivyObjekt.h"

class Monstrum : public PohyblivyObjekt
{
private:
	int hp;
	int maxhp;
public:
	int getHp() const;
	void setHp(int);
	int getMaxhp() const;
	void setMaxhp(int);
};
#endif

