#include "Hra.h"
#include "StatickyObjekt.h"
#include <iostream>

Hra::Hra()
{
	this->objekty = new Objekt * [10];
	int max = 10;
	int pocet = 0;
}

Hra::~Hra()
{
	for (int i = 0; i < pocet; i++)
	{
		delete objekty[i];
	}
	delete[] this->objekty;
}

void Hra::vlozObjekt(Objekt* o)
{
	if (max != pocet) {
		objekty[pocet] = o;
		pocet++;
	}
	else {
		Objekt** tempO = new Objekt * [max * 2];
		max *= 2;
		for (int i = 0; i < pocet; i++)
		{
			tempO[i] = objekty[i];
		}
		tempO[pocet] = o;
		pocet++;
		objekty = tempO;
	}
}

int* Hra::najdiIdStatickychObjektu(double xmin, double xmax, double ymin, double ymax)
{
	int* tmpArray = new int[pocet];
	int idCounter = 0;
	for (int i = 0; i < pocet; i++) {
		if (StatickyObjekt* so = dynamic_cast<StatickyObjekt*>(objekty[i])) {
			if ((so->getX() >= xmin && so->getX() <= xmax) && (so->getY() >= ymin && so->getY() <= ymax)) {
				tmpArray[idCounter++] = so->getId();
			}
		}
	}
	return tmpArray;
}

PohyblivyObjekt** Hra::najdiPohybliveObjektyVOblasti(double x, double y, double r)
{
	PohyblivyObjekt** tmpArray = new PohyblivyObjekt* [pocet];
	int moCounter = 0;
	for (int i = 0; i < pocet; i++) {
		if (PohyblivyObjekt* mo = dynamic_cast<PohyblivyObjekt*>(objekty[i])) {
			if ((pow(mo->getX() - x, 2) + pow(mo->getY() - y, 2)) < pow(r, 2)) {
				tmpArray[moCounter++] = mo;
			}
		}
	}
	return tmpArray;
}

PohyblivyObjekt** Hra::najdiPohybliveObjektyVOblasti(double x, double y, double r, double umin, double umax)
{
	PohyblivyObjekt** tmpArray = new PohyblivyObjekt* [pocet];
	int moCounter = 0;
	for (int i = 0; i < pocet; i++) {
		if (PohyblivyObjekt* mo = dynamic_cast<PohyblivyObjekt*>(objekty[i])) {
			if ((pow(mo->getX() - x, 2) + pow(mo->getY() - y, 2)) < pow(r, 2)) {
				if (mo->getUhelNatoceni() >= umin && mo->getUhelNatoceni() <= umax) {
					tmpArray[moCounter++] = mo;
				}
			}
		}
	}
	return tmpArray;
}
