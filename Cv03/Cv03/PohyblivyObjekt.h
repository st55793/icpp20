#ifndef __POHYBLIVYOBJEKT_H
#define __POHYBLIVYOBJEKT_H
#include "Objekt.h"

class PohyblivyObjekt : virtual public Objekt
{
private:
	double uhelNatoceni;
public:
	double getUhelNatoceni() const;
	void setUhelNatoceni(double);
};
#endif

