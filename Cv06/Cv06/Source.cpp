#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#include <string>
#include <iostream>
#include "GrowingContainer.h"

using namespace std;
int main() {
	GrowingContainer<string, 5> cont = GrowingContainer<string, 5>();
	for (int i = 0; i < 12; i++)
	{
		cont.add("String cislo: " + to_string(i));
		cout << cont[i] << endl;
	}
	cout << "Pocet prvku je: " << cont.count() << endl << endl;
	for (int i = 0; i < cont.count(); i++)
	{
		cout << cont[i] << endl;
	}
	cout << "Pocet prvku je: " << cont.count() << endl << endl;

	cont.remove(11);
	cont.addTo(11, to_string(20));
	for (int i = 0; i < cont.count(); i++)
	{
		cout << cont[i] << endl;
	}
	cout << "Pocet prvku je: " << cont.count() << endl << endl;

	return 0;
}