#ifndef _CAS_H_
#define _CAS_H_
#include "IComparable.h"

struct Cas : IComparable {
public:
	Cas(int _hodiny, int _minuty, int _sekundy);
	virtual int compareTo(IComparable* obj) const override;
	virtual std::string toString() const override;
private:
	int _hodiny;
	int _minuty;
	int _sekundy;
};

#endif
