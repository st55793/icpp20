#ifndef __OSOBA_H
#define __OSOBA_H

#include<string>
#include"Adresa.h"
#include"Datum.h"

struct Osoba {
private:
	std::string _jmeno;
	std::string _prijmeni;
	Adresa _trvaleBydliste;
	Datum _datumNarozeni;
public:
	Osoba();
	Osoba(std::string jmeno, std::string prijmeni, Adresa trvaleBydliste, Datum datumNarozeni);
	~Osoba();
	void setJmeno(std::string jmeno);
	void setPrijmeni(std::string prijmeni);
	void setTrvaleBydliste(Adresa trvaleBydliste);
	void setDatumNarozeni(Datum datumNarozeni);
	std::string getJmeno() const;
	std::string getPrijmeni() const;
	Adresa getTrvaleBydliste() const;
	Datum getDatumNarozeni() const;
};

std::ostream& operator<<(std::ostream& os, const Osoba& osoba);
std::istream& operator>>(std::istream& is, Osoba& osoba);

#endif
