#ifndef __UCTENKA_H
#define __UCTENKA_H

class Uctenka {
private:
	int cisloUctenky;
	double castka;
	double dph;
public:
	Uctenka();
	~Uctenka();
	void setCisloUctenky(int cisloUctenky);
	int getCisloUctenky();
	void setCastka(int castka);
	int getCastka();
	void setDph(double dph);
	double getDph();
};

#endif

