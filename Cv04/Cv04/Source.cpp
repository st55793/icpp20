#include <iostream>
#include "IObject.h"
#include "Double.h"
#include "Integer.h"
#include "Cas.h"
#include "IComparable.h"
using namespace std;

void SeraditPole(IComparable** pole, int velikostPole);

int main()
{

	IComparable** pole = new IComparable * [10];

	int randH, randM, randS;
	for (int i = 0; i < 10; i++)
	{
		randH = (std::rand() % (23 - 0 + 1)) + 0;
		randM = (std::rand() % (60 - 0 + 1)) + 0;
		randS = (std::rand() % (60 - 0 + 1)) + 0;
		pole[i] = new Cas{ randH, randM, randS };
	}

	for (int i = 0; i < 10; i++)
	{
		cout << pole[i]->toString() << endl;
	}

	SeraditPole(pole, 10);

	cout << "Setridene pole:" << endl;
	for (int i = 0; i < 10; i++)
	{
		cout << pole[i]->toString() << endl;
	}

	for (int i = 0; i < 10; i++)
	{
		delete pole[i];
	}
	delete[] pole;

	return 0;
}

void SeraditPole(IComparable** pole, int velikostPole)
{
	for (int i = 0; i < velikostPole - 1; i++) {
		for (int j = 0; j < velikostPole - i - 1; j++) {
			if (pole[j + 1]->compareTo(pole[j]) == 1) {
				IComparable* tmp = pole[j + 1];
				pole[j + 1] = pole[j];
				pole[j] = tmp;
			}
		}
	}
}
