#ifndef __HRA_H
#define __HRA_H
#include "Objekt.h"
#include "PohyblivyObjekt.h"

class Hra
{
private:
	Objekt** objekty;
	int max = 10;
	int pocet = 0;
public:
	Hra();
	~Hra();
	void vlozObjekt(Objekt*);
	int* najdiIdStatickychObjektu(double xmin, double xmax, double ymin, double ymax);
	PohyblivyObjekt** najdiPohybliveObjektyVOblasti(double x, double y, double r);
	PohyblivyObjekt** najdiPohybliveObjektyVOblasti(double x, double y, double r, double umin, double umax);
};
#endif
