#include <iostream>

using namespace std;

//Vytvo�te strukturu Trojuhelnik, kter� obsahuje slo�ky int a, b, c.
//Upravte prvn� program tak, aby vytvo�il objekt Trojuhelnik
// a do n�j na��tal jednotliv� hodnoty. Upravte test sestrojitelnosti, tak
//aby to nyn� byla funkce: bool lzeSestrojit(Trojuhelnik t).


typedef struct Trojuhelnik {
	unsigned int a;
	unsigned int b;
	unsigned int c;
} Trojuhelnik;

bool lzeSestrojit(Trojuhelnik t) {
	if ((t.a + t.b > t.c && t.a + t.c > t.b && t.b + t.c > t.a)) {
		return true;
	}
	else {
		return false;
	}
}


int main() {

	Trojuhelnik t;

	std::cout << "Zadejte stranu a: " << std::endl;
	std::cin >> t.a;

	std::cout << "Zadejte stranu b: " << std::endl;
	std::cin >> t.b;

	std::cout << "Zadejte stranu c: " << std::endl;
	std::cin >> t.c;

	if (lzeSestrojit) {
		unsigned int o = t.a + t.b + t.c;
		std::cout << "Obvod trojuhelniku je:" << o << std::endl;
		return 1;
	}

	std::cout << "Trojuhelnik nelze sestrojit (error exit code 1)" << std::endl;
	return 0;
}