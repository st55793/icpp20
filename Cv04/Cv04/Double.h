#ifndef _DOUBLE_H_
#define _DOUBLE_H_
#include "IObject.h"

struct Double : IObject {
public:
	Double(double value);
	virtual std::string toString() const override;
private:
	double value;
};

#endif
