#include <iostream>
using namespace std;

struct Trojuhelnik
{
	unsigned int a;
	unsigned int b;
	unsigned int c;
};

bool lzeSestrojit(Trojuhelnik* t) {
	if (!(t->a + t->b > t->c && t->a + t->c > t->b && t->b + t->c > t->a)) {
		return true;
	}
	else {
		return false;
	}
}


int main()
{
	unsigned int pocetTroj = 0;

	std::cout << "Kolik trojuhelniku:" << std::endl;
	std::cin >> pocetTroj;

	if (pocetTroj < 0) {
		std::cout << "Nelze zadat zaporne cislo" << std::endl;
		return 1;
	}

	Trojuhelnik* trojuhelniky = new Trojuhelnik[pocetTroj];

	for (; pocetTroj; --pocetTroj) {
		Trojuhelnik* t = &(trojuhelniky[pocetTroj - 1]);

		std::cout << "";
		std::cout << "Zadejte stranu a: " << std::endl;
		std::cin >> t->a;
		std::cout << "Zadejte stranu b: " << std::endl;
		std::cin >> t->b;
		std::cout << "Zadejte stranu c: " << std::endl;
		std::cin >> t->c;

		if (lzeSestrojit(t)) {
			std::cout << "Trojuhelnik nelze sestrojit!" << std::endl;
			continue;
		}

		unsigned int o = t->a + t->b + t->c;
		std::cout << "Obvod trojuhelniku je:" << o << std::endl;
	}

	delete[] trojuhelniky;
	return 0;

}