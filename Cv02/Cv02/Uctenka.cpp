#include "Uctenka.h"

Uctenka::Uctenka()
{
	this->cisloUctenky = 0;
	this->castka = 0;
	this->dph = 0;
}

Uctenka::~Uctenka()
{
}

void Uctenka::setCisloUctenky(int cisloUctenky)
{
	this->cisloUctenky = cisloUctenky;
}

int Uctenka::getCisloUctenky()
{
	return this->cisloUctenky;
}

void Uctenka::setCastka(int castka)
{
	this->castka = castka;
}

int Uctenka::getCastka()
{
	return this->castka;
}

void Uctenka::setDph(double dph)
{
	this->dph = dph;
}

double Uctenka::getDph()
{
	return this->dph;
}
