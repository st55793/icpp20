#include<iostream>
#include "TelefonniSeznam.h"

Model::TelefonniSeznam::TelefonniSeznam()
{
	this->_zacatek = nullptr;
}

Model::TelefonniSeznam::~TelefonniSeznam()
{
	PrvekSeznamu* prvekSeznamu = this->_zacatek;
	while (prvekSeznamu != nullptr) {
		PrvekSeznamu* tmp = prvekSeznamu->dalsi;
		delete prvekSeznamu;
		prvekSeznamu = tmp;
	}
}

void Model::TelefonniSeznam::pridejOsobu(Entity::Osoba o)
{
	PrvekSeznamu* novyPrvekSeznamu = new PrvekSeznamu{};
	novyPrvekSeznamu->dalsi = this->_zacatek;
	novyPrvekSeznamu->data = o;
	this->_zacatek = novyPrvekSeznamu;
}

std::string Model::TelefonniSeznam::najdiTelefon(std::string jmeno) const
{
	if (jmeno == "") {
		throw std::invalid_argument("Neplatny vstupni parametr");
	}
	PrvekSeznamu* prvekSeznamu = this->_zacatek;
	while (prvekSeznamu != nullptr) {
		if (prvekSeznamu->data.getJmeno() == jmeno) {
			return prvekSeznamu->data.getTelefon();
		}
		prvekSeznamu = prvekSeznamu->dalsi;
	}
	throw std::exception("Objekt nebyl nalezen");
}

std::string Model::TelefonniSeznam::najdiTelefon(int id) const
{
	if (id < 0) {
		throw std::invalid_argument("Neplatny vstupni parametr");
	}
	PrvekSeznamu* prvekSeznamu = this->_zacatek;
	while (prvekSeznamu != nullptr) {
		if (prvekSeznamu->data.getId() == id) {
			return prvekSeznamu->data.getTelefon();
		}
		prvekSeznamu = prvekSeznamu->dalsi;
	}
	throw std::exception("Objekt nebyl nalezen");
}

Model::TelefonniSeznam::PrvekSeznamu::PrvekSeznamu()
{
	this->dalsi = nullptr;
}

Model::TelefonniSeznam::PrvekSeznamu::~PrvekSeznamu()
{
}
